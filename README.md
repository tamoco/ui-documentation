## Tamoco documentation

Our documentation is (will be) avaiable within the user accounts and on docs.tamoco.com. 

### Overview

The documentation is controlled by [this JSON object](documentation.json).
Please do not change this directly without understanding the format and
restrictions for a valid JSON object. 

The documentation pages will download markdown formatted pages directly from
several bitbucket repositories and display it to the users. 

### Repositories

* https://bitbucket.org/tamoco/ui-documentation (Main repositories for pages updated by Tamoco)
* https://bitbucket.org/tamoco/api-docs (Updated by BSM)
* https://bitbucket.org/tamoco/tamoco-android-sdk (Updated by FS)
* https://bitbucket.org/tamoco/tamoco-ios-sdk (Updated by FS)
* https://bitbucket.org/tamoco/tamoco-unity-sdk (Updated by FS)


### How to edit

Navigate through the file structure to find the file you want to edit. Press
"Edit" (positioned almost at the top right corner) and make your edits. If
anyone edits a page in the same repository while you edit, your changes will be
lost. It's therefore a good idea to know what you're editing and keep a copy of
the markdown if possible. 

[dillinger.io](https://dillinger.io/) is a good tool to edit and preview markdown.
