## Consumer Data and Privacy Statement
Tamoco�s starting point is that the end user is in control, they must provide
clear consent for use of both their personal data and aggregate data relating
to their use of a service.  The best way of providing this control is to use
the tools provided by the main mobile operating systems and supplementing these
where required.  Tamoco does not request personal information from users and
never has access to a user's information such as email, phone number, address,
MAC address.

### How do users provide consent?
User consent is provided through the existing App Store and Play framework,
they provide location, and if requested, Bluetooth and WiFi permission through
their app settings, either at install or when using the app and it is relevant
to request the permission. These settings can be changed by the user at any
point.  Some partners additionally request specific permission to provide
location enabled offers and information as part of the first time app
registration process and store this setting on their user database. This can
then be accessed by the user via their app menu and changed at a later date.

Here's an example of the language used by a Tamoco app partner to provide
location triggered content:

> XXX uses your location when the app is not running so that we can 
> suggest relevant events and activities near to your current location.  
> Allow tailored recommendations based on where you are?  **Allow / Don't allow** 

All partners must describe what they do with user data in their Privacy Policy
for their app including if they provide anonymous aggregated data to third
parties for targeted advertising, usage analysis etc.  They may link to the
Tamoco Privacy Policy where explicitly stating the third parties that they
provide data to.

### Advertiser ID and Do Not Track:
Tamoco uses the existing iOS and Android Do Not Track feature to enable users
to opt out of third party data collection.  All users may reset their
advertiser ID at any time or change their setting to Do Not Track, information
on how to do this is included in the Tamoco Privacy Policy below.  On selecting
Do Not Track any data will be discarded and Tamoco will not store any data for
that device.  It is best practice to reference the Do Not Track function in the
privacy policy of any app that provides data to third parties.
 
### User data server location:
Tamoco uses Google cloud hosting in the EU for core infrastructure and non
personally identifiable data is stored here.  We have the facility to store
personally identifiable data on behalf of our customers on Amazon Web Service
servers, again within the EU.  In this case, we are acting as the data processor
for our clients and do not have access to the unencrypted user data.
 
### EU data protection registration:
Tamoco is a London based, UK registered company, registered with the ICO in the
UK and has validated our architecture for GDPR compliance.  Further detail on
how Tamoco processes data can be found in our Privacy Policy:
https://www.tamoco.com/privacy
