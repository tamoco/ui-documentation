

# What data does the Tamoco SDK capture 
The Tamoco SDK collects non-identifiable data about the consumers mobile device
and interactions with sensors like Bluetooth beacons or Wifi. 

The end consumer will always be able to control which permissions are given to
the app and thereby the Tamoco SDK. For example:

1. The user can disable Bluetooth or WiFi thereby not making it possible to save any WiFi or Beacon-related data. 
2. The user can use "Do not track" thereby disabling the Tamoco SDK from reading the device ID.

The following data tables are split between different type of data:

 - Device information: This is data unique to the consumer device.
 - Sensor information: Data unique to the physical sensor or geofence. 
 - Processed data: Attributes derived directly from the information collected above.

### Device information

| Name           | Description     | Example    | 
|--------------- |:----------------|:-----------|
| `id` | The advertisment id for the phone. | 6D92078A-8246-4BA4-AE5B-76104861E7DC |
| `ip` | IP address. | 12.13.14.15 |
| `type` | Device type | Phone, Tablet or Unknown |
| `make` | Extracted from the phones user agent. Defines the device manufacturer | Samsung|
| `model` | The device model number.  | SM-A300FU |
| `language` | The device default language | en_GB |
| `os` | The type of operating system. iOS or Android | Android |
| `os_version` | The operating system version number  | 10.0 |
| `hw_version` | Information about the device | iPhone 6 Plus |
| `screen_width` | The width of the screen in pixels.  | 2,560 |
| `screen_height` | The height of the screen in pixels. | 1,440 |
| `bluetooth` |  Bluetooth state on the deice. True means Bluetooth is on, False otherwise. | True |
| `wifi` | WiFi state on the deice. True means WiFi is on, False otherwise. | True |
| `location` | Device location (GPS) state on the deice. True means device location is activated, False otherwise. | True |
| `battery` | The percentage of the battery left on device.  | 99 |
| `battery_health` | The heealth of the battery | 1 |
| `battery_saver` | Is the device in battery saver mode | false |
| `battery_technology` | Battery type | Li-ion |
| `battery_temperature` | The temperature of the battery | 26 |
| `battery_voltage` | The volage of the battery | 4181 |
| `timezone` | The timezone of the device.  | Europe/London |


### Location data
| Name           | Description     | Example    |
|--------------- |:----------------|:-----------|
| `latitude` | The north-south position of a point on the Earth's surface. First part of the device location. | 51.518903 | 
| `longitude` | The east-west position of a point on the Earth's surface. Second part of the device location. | -0.128506 | 
| `horizontal_accuracy` | The measuring accuracy of the device location as reported by the os. In meters | 10.3 |
| `vertical_accuracy` | The measuring accuracy of the device location as reported by the os. In meters | 3 |
| `bearing` | The direction in which the device is moving. 
| `speed` | The speed of the device as reported by the os. Measured in metres per second. | 1 |
| `speed_accuracy` | Accuracy of speed. Measured in metres. | 1 | |`altitude` | The altitude of the device as reported by the os. In metres | 23 |
| `event_type` | What triggered the event. Walking in and out of a geofence will trigger both a `GEO_ENTER` and a `GEO_EXIT` event. | `GEO_ENTER`, `GEO_EXIT`, `WIFI_CONNECT`, `WIFI_DISCONNECT`    |
| `floor` | The floor level of the device as inferred by altitude | 2 |
| `trigger_proximity` | The distance of the device from the centre of the geofence inventory. Measured in metres. | 4 |
| `sdk_ts` | The timestamp of when the SDK collected data | 1506695667 |
| `trigger_rssi` | The beacon or wifi signal strengt. | 9
| `inv_type` | The type of inventory used to measure location. | `BEACON` `WIFI` `GEOFENCE` |

### Sensor data

The below table lists the data the Tamoco SDK uses for attribution. Given the direct relationships between Tamocco and the sensor owners, more information is stored for each sensor. However, only the below is used to attribute a consumer device with the sensor. 

| Name           | Description     | Example    |
|--------------- |:----------------|:-----------|
| `wifi ssid` | The name of the wifi. | O2 Wifi |
| `wifi_bbsid` | The mac address of the router | 00:08:5C:00:00:01 |
| `wifi_channel_width` | The width of the wifi channel (android only) | 1 |
| `wifi_frequency` | The frequency length of the wifi router (android only) | 5200 |
| `mac_address` | The mac address of the wifi (android only) | cc:33:bb:b8:71:3f |
| `wifi_rssi` | The signal strength of the wifi (android only) | -80 |
| `wifi_operator_name` | The operating name of the wifi router | netgear |
| `beacon_major` | Major and Minor values are numbers assigned to your iBeacons, in order to identify them with greater accuracy than using UUID alone. | 2 | |
| `beacon_minor` | Major and Minor values are numbers assigned to your iBeacons, in order to identify them with greater accuracy than using UUID alone. | 4 |
| `beacon_id` | Universally Unique Identifier | f7826da6-4fa2-4e98-8024-bc5b71e0893e |

### Application data
| Name           | Description     | Example    |
|--------------- |:----------------|:-----------|
| `app_id` | Unique identifieer of the app iused to capture data. | `fb:app` |
| `app_bundle_id` | Unique identifier used to indentify the bundle ID of the app. | `com.facebook.ios`
| `app_name` | The name of the app used to capture data. | `facebook`
| `app_version` | The version indentifier of the app. | 2.4 |
| `app_sdk_version` | The version of the SDK used to capture data. | 1.1 |

### Processed data
| Name           | Description     | Example    |
|--------------- |:----------------|:-----------|
| `dwell_time` | The time measured between an enter and exit event. This is used to discard place visits with too short a duration. | 85 seconds |
| `place` | Attribution to the location where a number of events happened. | Starbucks coffee |
