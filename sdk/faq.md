# FAQ

## What is the minimum required version? 

The Tamoco iOS SDK supports iOS 8 and above. The SDK requires a minimum Android
SDK of 14 (Android 4.0 Ice Cream Sandwich). The maximum currently supported is
26 (Android Oreo 8.0).

## My app supports earlier versions, can I implement? 

Yes, the Tamoco SDK will automatically inspect the device android version and
disabled services that are not compliant with some older versions of Android. 

## What size is the SDK? 

The Android SDK is 150KB. The iOS SDK is 3.4MB. The iOS SDK download is larger,
but the mentioned sizes are the storage taken up on a device. 

## Does the SDK drain battery?

Our SDK has been developed over multiple years to ensure that it is battery
efficient. There is always a trade-off between efficiency and responsiveness.

Tamoco's default SDK settings favor efficiency to minimise the effect on the
end user and protect our app partners. We provide our partners with additional
controls from within our platform to refine those settings at any time, without
updating the app.

## How do users provide consent 

User consent is provided through the existing App Store and Play framework -
they provide location and push notification permissions required to activate
the Tamoco proximity capability.

Example opt-in language is as follows:
 >XXX uses your location when the app is running so that we can suggest relevant events and activities near to your current location. Allow tailored recommendations based on where you are? 
 **Allow / Don't allow** 

## Can my users opt-out of this service?

Tamoco uses the existing iOS and Android Do Not Track feature to enable users
to opt out of third-party data collection.

All users may reset their advertiser ID at any time or change their settings to
Do Not Track.  On selecting Do Not Track data will be discarded and Tamoco will
not store any data for that device.

## How will this affect my privacy policy? 

All partners must describe what they do with user data in their privacy policy
for their app including if they provide anonymous aggregated data to third
parties.

We recommend that all apps reference the Do Not Track function in the App
privacy policy.

## Is this legal?

Tamoco is compliant with current EU and UK legislation, the gold standard
globally on consumer data and privacy protection.

## Where is the Data stored? 

Tamoco stores non personally identifiable user data in EU hostel instances of
the Google Cloud Service. Tamoco has the ability to store encrypted personal
data on behalf of our clients in EU hosted Amazon AWS instances.

## Do you provide boilerplate terms and conditions?

As every app is different we cannot provide individual, off the shelf terms and
conditions. We can, however, point to examples of good practice from our
current partners. Voucher cloud are are a good example.
