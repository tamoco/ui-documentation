# The Tamoco SDK

The Tamoco SDK is a proximity SDK enabling app owners to monetize location data
and provides a toolkit for improving user experience based on highly
precise location data. 

## Technology

The SDK is available as a native iOS and Android SDK or as a Unity wrapper. The
SDK relies on access to device location (GPS), wifi and Bluetooth Beacons
(supporting both iBeacon and Eddystone).

## Full control

As the app owner, you're able to control the behaviour of the SDK to suit your user base. This includes access to control the amount of location pings gathered, foreground and background operation and more. Please review
the specific integration pages for the full description of available options. 