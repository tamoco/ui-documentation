```php
$APP_ID = '12345';     // Our provided APP ID
$SECRET = 't0Psec3et'; // Our provided secret

// Generate a 8-byte cryptographically random salt. Refresh on every use. Do not store it.
$salt = pack('P', time());
$hmac = hash_hmac('sha512', $salt, $SECRET, true);

$auth_header = 'Token ' . $APP_ID . '/' . base64_encode($salt . substr($hmac, 0, 16));
echo($auth_header);
```

Example:

```php
$APP_ID = "12345"
$SECRET = "t0Psec3et"
$timestamp = 1500000000 // 2017-07-14 02:40:00 +0000 UTC
```

Output:

```
Token 12345/AC9oWQAAAACiSxeGIk2hwXkQy9WlK1OH
```
