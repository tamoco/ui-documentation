```ruby
require 'openssl'
require 'securerandom'
require 'base64'

APP_ID = "12345"
SECRET = "t0Psec3et"

salt = [Time.now.to_i].pack('Q<')
hmac = OpenSSL::HMAC.new(SECRET, OpenSSL::Digest.new('sha512'))
hmac.update(salt)
hash = hmac.digest

puts "Token " + APP_ID + '/' + Base64.encode64(salt + hash[0, 16])
```

Example:

```ruby
APP_ID = "12345"
SECRET = "t0Psec3et"
timestamp = 1500000000 # => 2017-07-14 02:40:00 UTC
```

Output:

```
Token 12345/AC9oWQAAAACiSxeGIk2hwXkQy9WlK1OH
```
