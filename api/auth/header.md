# Token authentication

The proposal to authenticate requests via salted and hashed tokens, passed in the `Authorization` HTTP header. Example header:

```
Token 1/LETM3IN/AAA4ZQ618aSpc8/hMG6Ffo5T
```

which has the format:

```
Token <app_id>/<token>
```

Please see our reference implementations:

 * [Ruby](ruby.md)
 * [Google Go](go.md)
 * [Python](python.md)
 * [PHP](php.md)

Reference example:

```
APP_ID = "12345"
SECRET = "t0Psec3et"
salt = "ABCDEFGHIJKLMNOP"
```

Correct `Authorization` header value:

```
Token 12345/QUJDREVGR0hJSktMTU5PUL1Xo73V6Kcd//p2YVOAgRE=
```
