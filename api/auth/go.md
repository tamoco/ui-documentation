```go
package main

import (
	"crypto/hmac"
	"crypto/sha512"
	"encoding/base64"
	"encoding/binary"
	"fmt"
	"time"
)

const (
	appID = "12345"
	secret = "t0Psec3et"
)

func makeToken(salt []byte) string {

	// Hash the salt with HMAC sha512 hash using our shared secret
	enc := hmac.New(sha512.New, []byte(secret))
	_, _ = enc.Write(salt)
	hash := enc.Sum(nil)

	res := make([]byte, len(salt)+16)
	copy(res, salt)
	copy(res[len(salt):], hash[:16])

	// Encode with base64
	return base64.StdEncoding.EncodeToString(res)

}

// Generate a cryptographically secure 16-byte UUID
func newUUID() []byte {
	ts := time.Now().Unix()
	salt := make([]byte, 8)
	ts = 1500000000
	binary.LittleEndian.PutUint64(salt, uint64(ts))
	return salt

}

func main() {
	// Create a 16-byte cryptographically secure salt (e.g. a UUID)
	salt := newUUID()

	// Generate token
	fmt.Println("Token " + appID + "/" + makeToken(salt))
}
```

Example:

```go
appID =: "12345"
secret := "t0Psec3et"
time := 1500000000 // 2017-07-14 02:40:00 +0000 UTC
```

Output:

```
Token 12345/AC9oWQAAAACiSxeGIk2hwXkQy9WlK1OH
```
