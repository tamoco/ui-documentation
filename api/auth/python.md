```python
# Works across py2 and py3. Cast output of authToken to string as appropriate.
import base64
import hashlib
import hmac
import os
import struct
import time

def authToken(app_id, api_secret, timestamp):
    salt = struct.pack('<Q', timestamp)
    hash = hmac.new(api_secret, salt, hashlib.sha512).digest()

    return b'Token ' + app_id + b'/' + base64.b64encode(salt + hash[:16])

# Call with:
# authToken(APP_ID, API_SECRET, time.now())
```

Reference output for:

```
>>> APP_ID = b'12345'
>>> API_SECRET = b't0Psec3et'
>>> authToken(APP_ID, API_SECRET, 1500000000)
b'Token 12345/AC9oWQAAAACiSxeGIk2hwXkQy9WlK1OH'
```
