# Tamoco API
Using the Tamoco API you can control every aspect of your account including
management of apps, sensors, campaigns and places. We also have documentation
for how to supply data to the Tamoco platform.

## Authentication

Tamoco offers two different ways to authenticate. As data supply can be
millions of requests we offer a fast [token authentication](auth/header.md).

For everything else, we offer OAuth. Please see the documentation for more
details and contact us for API access. 

## Available APIs:

- Apps - Manage and control your apps. Primarily useful for audience partners.
- Sensors - Manage and control sensors (GeoFences, WiFi, Beacons, NFC tags and QR codes). Useful for sensor partners and for audience partners using the Tamoco services to engage users.
- Places - Manage and control PoI (Points of Interest)

See also our guide for sending data in real time to the Tamoco platform.

## Upcomming documentation

- Extracing data from the Tamoco platform.
- Sending data in bulk to the Tamoco platform.
